package src;

public class Main {
    public static void main(String[] args) {
        var triangle = new Triangle(3.0, 4.0, 5.0);

        System.out.println("Area = " + triangle.area());
        System.out.println("Perimeter = " + triangle.perimeter());
    }
}
